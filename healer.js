let healer = {
    name: null,
    healthPoints: 151,
    isAlive: function() {
        if (this.healthPoints > 0) {
            return true;
        }
        return false;
    },

    isDead: function () {
        return !this.isAlive();
    },
    attack: function (target) {
        if (typeof target !== 'object' || target === null) {
            console.warn("Impossible d'attaqué une cible qui n'est pas un personnage");
            return;
        }
        if (target === this) {
            console.warn("Impossible de t'attaquer toi même !");
            return;
        }
        if (!this.isAlive()) {
            console.warn("Impossible d'attaquer quand on est mort !")
            return;
        }

        if (!target.isAlive()) {
            console.warn("Impossible d'attaquer un mort !")
            return;
        }

        console.log(this.name + " essaye d'absorder la vie de " + target.name)

        if (Math.random() <= 0.9) {
            target.healthPoints -= 50;
            this.healthPoints += 50;
            console.log( this.name + ' Absorbe la vie  !')
        } else {
            console.log(this.name + ' a raté son coup')
        }
    },
    defence: function () {
        if (target === this)
        this.healthPoints += 100;
        console.log(this.name + ' a recu 100 PV')
    },
};
